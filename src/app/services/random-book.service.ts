import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Books} from '../models/books';
import {Book} from '../models/book';


@Injectable({
  providedIn: 'root'
})
export class RandomBookService {
  url = './assets/books.json';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:typedef
  getData(text: string, page: number): Observable<Books> {
    return this.http.get<Books>(this.url) /* + '&s=' + text + '&page=' + page);*/
  }

  getDataById(id: string): Observable<Book> {
    return this.http.get<Book>(this.url + '&i=' + id + '&plot=full');
  }
  getDataBook(){
    return this.http.get('assets/books.json')
  }
}
