import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
cartBooks = [
  {id: 1, title: 'test1', price: 100},
  {id: 2, title: 'test2', price: 500},
  {id: 3, title: 'test3', price: 800},
  {id: 4, title: 'test4', price: 300}
    ];

cartTotal = 0;
  constructor() { }

  ngOnInit(): void {
    this.cartBooks.forEach(book =>{
      this.cartTotal += book.price
    })
  }

}
