import { Component, OnInit, OnDestroy } from '@angular/core';
import {Book} from '../models/book';
import {Books} from '../models/books';
import { HttpClient} from '@angular/common/http';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {RandomBookService} from '../services/random-book.service';
import {HttpService} from '../http.service';
import {MessengerService} from '../services/messenger.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [HttpService]
})
export class  MainComponent implements OnInit {

  books: Book[];


  constructor(private Activatedroute: ActivatedRoute,
              private router: Router,
              private movieService: RandomBookService,
              private httpService: HttpService,
              private msg: MessengerService) {
    this.books = new Array<Book>();
  }


  // tslint:disable-next-line:typedef
  ngOnInit() {

    this.httpService.getData().subscribe(data => this.books = data["books"]);

  }
  handleAddToCart() {

    this.msg.sendMsg(this.books)
  }
}
