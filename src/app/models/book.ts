export class Book {
  title: string;
  id: number;
  poster: string;
  year: number;
  price: number;
}
